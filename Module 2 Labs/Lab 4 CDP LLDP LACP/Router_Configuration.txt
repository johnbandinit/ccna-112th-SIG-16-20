######ROUTER CONFIGURATION######
!
enable
! 
conf t
!
hostname R1
!
cdp run
!
lldp run
!
int gi0/0
 description ///TO SW1\\\
 no shut
 exit
!
int gi0/0.10
 description ///SALES VLAN 10\\\
 encap dot1q 10
 ip address 22.3.4.129 255.255.255.192
 exit
!
int gi0/0.20
 description ///ACCOUNTING VLAN 20\\\
 encap dot1q 20
 ip address 22.3.4.1 255.255.255.128
 exit
!
int gi0/0.30
 description ///MGMT VLAN 30\\\
 encap dot1q 30
 ip address 22.3.4.193 255.255.255.240
 exit
!
int gi0/0.40
 description ///VOICE VLAN 40\\\
 encap dot1q 40
 ip address 22.3.4.209 255.255.255.240
 exit
!
do wr
!
