#####Switch 2 Configuration#####
!
en
!
conf t
!
cdp run
!
lldp run
!
hostname SW2
!
vlan 10 
 name SALES
 exit
!
vlan 20
 name ACCOUNTING
 exit
!
vlan 30
 name MGMT
 exit
!
vlan 40
 name VOICE
 exit
!
int gi0/1
 description ///ACCOUNTING DEPT\\\
 switchport mode access
 switchport access vlan 20
 no cdp enable
 exit
!
int gi0/0
 description ///SALES DEPT\\\
 switchport mode access
 switchport access vlan 10
 switchport voice vlan 40
 exit
!
int gi1/1
 description ///TRUNK TO SW3\\\
 switchport trunk encap dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int gi1/2
 description ///TRUNK TO SW3\\\
 switchport trunk encap dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int gi3/0 
 description ///TRUNK TO SW1\\\
 switchport trunk encap dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int gi3/1 
 description ///TRUNK TO SW1\\\
 switchport trunk encap dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int vlan 1
 shutdown
 exit
!
int vlan 306
 description ///MGMT SVI\\\
 ip address 22.3.4.195 255.255.255.240
 no shut
 exit
!
ip default-gateway 22.3.4.193 
!
int ra gi3/0 - 1
 channel-group 5 mode active
 exit
!
int ra gi1/0 - 1
 channel-group 1 mode on
 exit
!
do wr
!