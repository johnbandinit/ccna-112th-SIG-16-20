######Switch 1 Configuration#######
!
en
!
conf t
!
cdp run
!
lldp run
!
hostname SW1
!
vlan 10 
 name SALES
 exit
!
vlan 20
 name ACCOUNTING
 exit
!
vlan 30
 name MGMT
 exit
!
vlan 40
 name VOICE
 exit
!
int gi3/0 
 description ///TRUNK TO SW2\\
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int gi3/1 
 description ///TRUNK TO SW2\\
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int gi2/0
 description ///TRUNK TO SW3\\\
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int gi2/1
 description ///TRUNK TO SW3\\\
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int gi0/0
 description ///TRUNK TO RTR\\\
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int vlan 1
 shutdown
 exit
!
int vlan 30
 description ///MGMT SVI\\\
 ip address 22.3.4.194 255.255.255.240
 no shut
 exit
!
ip default-gateway 22.3.4.193 
!
int ra gi2/0 - 1
 channel-group 10 mode desirable
 exit
!
int ra gi3/0 - 1
 channel-group 5 mode active
 exit
!
do wr
!